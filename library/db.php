<?php
namespace library;

use PDO;
use core\classes\Config;



class Db
{


    private static $instance;

    public static function getConnect(){
        if(self::$instance === NULL){
            $config = Config::Load('database');

            self::$instance = new PDO(
                "mysql:host={$config["host"]};port={$config["port"]};dbname={$config["dbname"]};charset={$config["charset"]}",
                $config["user"],
                $config["pass"],
                [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,

                ]

            );

            return self::$instance;
        }else{
            return self::$instance;
        }

    }

    private  function __construct(){}
    private function __clone(){}



}
