-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 04 2018 г., 20:58
-- Версия сервера: 5.7.20
-- Версия PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `store`
--

-- --------------------------------------------------------

--
-- Структура таблицы `articles`
--

CREATE TABLE `articles` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `articles`
--

INSERT INTO `articles` (`id`, `title`, `description`, `created_at`, `updated_at`) VALUES
(1, 'New Brand', 'some description', '2018-05-22 14:37:30', '2018-05-22 14:37:30'),
(2, 'New Titles', 'some description', '2018-05-22 15:17:28', '2018-05-22 15:17:28');

-- --------------------------------------------------------

--
-- Структура таблицы `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `publish` tinyint(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `categories`
--

INSERT INTO `categories` (`id`, `name`, `description`, `publish`) VALUES
(1, 'Guns', 'some description', 1),
(2, 'Machine Gun', 'some description', 1),
(3, 'Grenades', 'some description', 1),
(4, 'Grenade Launcher', 'some description', 1),
(5, 'Drugs', 'some description', 0),
(6, 'Cold Steel', 'some description', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `phone` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `user_id`, `phone`, `created_at`, `updated_at`) VALUES
(3, 1, '0930050608', '2018-05-18 18:32:35', '2018-05-18 18:32:35'),
(4, 2, '0972093233', '2018-05-18 18:38:22', '2018-05-18 18:38:22'),
(7, 3, '0503251278', '2018-05-21 16:16:29', '2018-05-21 16:16:29'),
(8, 4, '0633553315', '2018-05-22 11:55:48', '2018-05-22 11:55:48');

-- --------------------------------------------------------

--
-- Структура таблицы `orders_to_products`
--

CREATE TABLE `orders_to_products` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `price` decimal(10,0) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders_to_products`
--

INSERT INTO `orders_to_products` (`id`, `order_id`, `product_id`, `price`) VALUES
(1, 7, 11, '500'),
(2, 8, 11, '350');

-- --------------------------------------------------------

--
-- Структура таблицы `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `articul` varchar(12) NOT NULL,
  `brand` varchar(255) NOT NULL,
  `image_path` varchar(255) NOT NULL,
  `description` longtext NOT NULL,
  `price` decimal(10,0) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `publish` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `products`
--

INSERT INTO `products` (`id`, `name`, `articul`, `brand`, `image_path`, `description`, `price`, `category_id`, `created_at`, `updated_at`, `publish`) VALUES
(11, 'Makarov Gun', 'TT01RU', 'TT', 'blog/admin/products-create.php', 'some description', '300', 1, '2018-05-18 17:55:33', '2018-05-18 17:55:33', 1),
(12, 'Forty-Nine', 'FN07BG', 'FN', 'blog/admin/products-create.php', 'some description', '300', 1, '2018-05-18 19:31:58', '2018-05-18 19:31:58', 1),
(15, 'FNX', 'FNX01BG', 'FNX', 'blog/admin/products-create.php', 'some description', '480', 1, '2018-05-21 19:12:21', '2018-05-21 19:12:21', 1),
(17, 'AK - 47', 'AK007RU', 'Kalashnikov', 'blog/admin/products-create.php', 'some description', '200', 2, '2018-05-21 19:13:50', '2018-05-21 19:13:50', 1),
(36, 'M16', 'A&K M16A3', 'A&K', 'blog/admin/products-create.php', 'some description', '250', 2, '2018-05-22 12:50:39', '2018-05-22 12:50:39', 1),
(37, 'CA', 'CA AUG A1', 'AUG', 'blog/admin/products-create.php', 'some description', '260', 2, '2018-05-22 13:33:18', '2018-05-22 13:33:18', 1),
(39, 'A&K LR-300 LONG', 'LR-300', 'A&K', 'blog/admin/products-create.php', 'some description', '280', 2, '2018-05-22 13:36:34', '2018-05-22 13:36:34', 1),
(54, 'Grenade F1', 'F1BG', 'Standard', 'blog/admin/products-create.php', 'some description', '50', 3, '2018-05-23 23:08:07', '2018-05-23 23:08:07', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `phone`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Muhamed', 'muhamed@ukr.net', '093-056-77-23', 'qwerty', '2018-05-19 07:23:17', '2018-05-19 07:23:17'),
(2, 'Ashot', 'ashot@ukr.net', '097-056-65-23', 'asd123', '2018-05-19 07:31:59', '2018-05-19 07:31:59'),
(3, 'Husein', 'sadamhusein@gmail.com', '404-435-368-99', 'qazwsx', '2018-05-22 12:00:50', '2018-05-22 12:00:50'),
(4, 'Gera', 'gera@gmail.com', '0970293132', 'qweasd', '2018-05-23 12:54:38', '2018-05-23 12:54:38');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Индексы таблицы `orders_to_products`
--
ALTER TABLE `orders_to_products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`),
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category_id` (`category_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `articles`
--
ALTER TABLE `articles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT для таблицы `orders_to_products`
--
ALTER TABLE `orders_to_products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT для таблицы `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Ограничения внешнего ключа таблицы `orders_to_products`
--
ALTER TABLE `orders_to_products`
  ADD CONSTRAINT `orders_to_products_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`id`),
  ADD CONSTRAINT `orders_to_products_ibfk_2` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`);

--
-- Ограничения внешнего ключа таблицы `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
