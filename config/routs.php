<?php
return [
	'' => [
		'controller' => 'main',
		'action' => 'index',
	],
	'contact' => [
		'controller' => 'main',
		'action' => 'contact',
	],
	'about' => [
		'controller' => 'main',
		'action' => 'about',
	],
	'news' => [

		'controller' => 'news',
		'action'=> 'index',
			
	],
	'shop' => [

		'controller' => 'shop',
		'action'=> 'index',
			
	],
	'shop/add' => [

		'controller' => 'shop',
		'action'=> 'add',
			
	],
	'blog' => [
         'controller' => 'blog',
		'action'=> 'index',

	],
	'blog/article{id:\d+}' => [
         'controller' => 'blog',
		'action'=> 'index',

	],
	'admin' => [
        'controller' => 'admin',
		'action'=> 'admin',

	],
	'admin/categories' => [
        'controller' => 'admin',
		'action'=> 'categories',

	],
	'admin/addCategories' => [
        'controller' => 'admin',
		'action'=> 'addCategories',

	],

];