<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 07.06.2018
 * Time: 15:00
 */

namespace  core\classes;

class Config
{

    public static function Load($name)
    {
        return include 'config/'.$name.".php";
    }
}
