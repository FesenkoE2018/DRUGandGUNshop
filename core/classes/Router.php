<?php
/**
* 
*/

namespace core\classes;



class Router
{


	protected $routes = [];
	public $params = [];
	public function __construct(){
		$arr = Config::Load('routs');
		foreach ($arr as $key => $val) {
		 	$this->add($key, $val);
		 } 

	}

	public function add($route, $params)
	{

		$route = preg_replace('/{([a-z]+):([^\}]+)}/', '(?P<\1>\2)', $route);

		$route = '#^'.$route.'$#';

		$this->routes[$route] = $params;

	}
	public function match()
	{
		 $url = trim($_SERVER['REQUEST_URI'], '/');
		 foreach ($this->routes as $route => $params) {
            if (preg_match($route, $url, $matches)) {
                foreach ($matches as $key => $match) {
                        if (is_int($match)) {
                            $match =  $match;
                        }
                        $params[$key] = $match;
                    
                }
                $this->params = $params;
                return true;
            }
        }
		 return false;
	}
	public function run()
	{
		if($this->match()){
			$path = '\controllers\\'.ucfirst($this->params['controller']);
			
			if(class_exists($path)){
				$action = $this->params['action'].'Action';
				if(method_exists($path, $action)){
					$controller = new $path($this->params);
                    $controller->$action();



				}
				
			}else{
				echo 'Not founded'.$path; 
			}
		}else{
			echo 'Маршрут не найден';
		}
		
	}

}