<?php

/**
* 
*/
namespace core\classes;

class View 
{
	public $path;
	public $route;
	public $layout = 'default';

	public function __construct($route){
		$this->route = $route;
		$this->path = $route['controller'].'/'.$route['action'];
	

	}
	public function render($data=[]) {
		$path = 'view/'.$this->path.'.php';
		extract($data);
		if (file_exists($path)) {
			ob_start();
			require $path;
			$content = ob_get_clean();
			require 'view/layouts/'.$this->layout.'.php';
		}
	}

}