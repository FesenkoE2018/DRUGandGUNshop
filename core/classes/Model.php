<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 07.06.2018
 * Time: 18:11
 */

namespace core\classes;

use library\Db;


class Model {
    static $db;


    public function __construct()
    {

        self::$db = Db::getConnect();


    }

    public function getAll($table, $where = "1")
    {
        $stmt = self::$db->prepare("SELECT * FROM  {$table} WHERE {$where}");
        $stmt->execute();
        return $stmt->fetchAll();
    }

    public function getColomn($table, $data = [], $where = "1")
    {
        $stmt = self::$db->prepare("SELECT " . implode(",", $data) . " FROM  {$table} WHERE {$where}");
        $stmt->execute($data);
        return $stmt->fetchAll();
    }

    public function insert($table, $data)
    {
        $keys = array_keys($data);
        $q = "INSERT INTO {$table} (`" . implode("`,`", $keys) . "`)";
        $q .= "VALUES (:" . implode(", :", $keys) . ")";
        $sth = self::$db->prepare($q);
        $sth->execute($data);
        return self::$db->lastInsertId();
    }

    public function update($table, $data = [], $id)
    {
        $str = [];
        foreach ($data as $key => $value) {
            $str[]= "`" . $value . "`=?";
        }
        $q = "UPDATE {$table} SET " . implode(", ", $str);
        $q .= "WHERE `id`=" . $id;
        $sth = self::$db->prepare($q);
        return $sth->execute($data);
    }

    /**
     * @param $table
     * @param $id
     * @return bool
     */
    public function delete($table, $id) {
        $q = "DELETE FROM {$table} WHERE id = " . $id;
        $sth = self::$db->prepare($q);
        return $sth->execute();
    }

}
