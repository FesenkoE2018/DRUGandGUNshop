<?php 
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title><?= $title ?></title>
    <link rel="icon" type="image/png" href="/asset/img/favicon.png">
    <link href="/asset/style.css" rel="stylesheet">
    <link href="/asset/stylefis.css" rel="stylesheet">
</head>

<body>
<div class="wrapper">
    <header>
    
       <nav class="panelTop">
    <ul>
        <li><a href="#">My account</a> </li>
        <li><a  class="bascet" href="/index.php?action=bascet">
                <span class="countItem">
                    <?=  ($_SESSION['counter']) ?  $_SESSION['counter'] :  "0";?></span>
            </a> </li>
    </ul>

</nav>
<nav class="mainMenu">
    <a class="logo" href="/">
        <img src="asset/img/logo.png">
    </a>
    <div class="burger"></div>
    <ul>
        <li><a href="/shop">Shop</a></li>
        <li><a href="/about">About</a> </li>
        <li><a href="/blog">News $ Blog</a> </li>
        <li><a href="/contact">Contact</a> </li>
        <li><a href="/shop/add">Add</a> </li>
        <li><a href="http://DRUGandGUNshop/admin/">Admin Panel</a> </li>
    </ul>
</nav>

    </header>

    <main role="main" class="container">
        <div class="containerMain">
            <?php echo $content; ?>
        </div>
    </main>
   <script src="/asset/jquery-3.1.1.js"></script>
<script src="/asset/main.js"></script>

<footer>
    <div class="footer">


    <ul>
        <li><a href="/shop">Shop </a></li>
        <li><a href="/about">About</a> </li>
        <li><a href="/blog">News $ Blog</a> </li>
        <li><a href="/contact">Contact</a> </li>
        <li><a href="/shop/add">Add</a> </li>
    </ul>
    </div>
</footer>

</div>
</body>
</html>
