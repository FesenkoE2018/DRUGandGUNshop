<h2 class="title-admin">Categories panel</h2>
<div class="container">
    <div class="products-create-tablet">
        <h3 class="products-create-tablet__text">Addition category table</h3>
        <form action="/admin/addCategories" method="post">
            <table>
                <tr>
                    <td><label for="nameCategory">name:</label></td>
                    <td><input class="products-create__field" type="text" name="name_categories"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['name'] : "" ?>" id="nameCategory"
                               required></td>
                </tr>
                <tr>
                    <td><label for="descriptionCategories">description:</label></td>
                    <td><input class="products-create__field" type="text" name="description_categories"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['description'] : "" ?>"
                               id="descriptionCategories"
                               required></td>
                </tr>
                <tr>
                    <td><label for="publishCategories">publish:</label></td>
                    <td><input class="products-create__field" type="number" min="0" max="1" name="publish_categories"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['publish'] : "" ?>" id="publishCategories"
                               required></td>
                </tr>
                <input type="hidden" name="id" value="<?= !empty($_GET['id']) ? $_GET['id'] : "" ?>">
            </table>
            <p><input type="submit" name="submit" value="Add category"></p>
            <p><a href="http://drugandgunshop/admin/" class="back-admin">back to Admin-Panel</a></p>
        </form>
    </div>

</div>
