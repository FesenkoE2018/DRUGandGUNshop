<?php
session_start();
require_once "library/controller.php"; ?>
<!DOCTYPE html>
<html lang="en">

<?php require_once("include/header.php") ?>
<body>
<header>
    <?php require_once("include/menu.php") ?>
</header>
<main>
    <?php require_once $page; ?>
</main>
<?php require_once("include/footer.php") ?>
</body>
</html>

