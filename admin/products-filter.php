<?php
include_once 'DB.php';

if (isset($_POST['products_filter'])) {
    $priceOrder = $_POST['price_filter'];
    $priceBetween = $_POST['price_between'];
    $search = strtolower($_POST['search']);
}

if (!empty($priceBetween[0])) {
    $sqlBetween = " WHERE (`price` BETWEEN $priceBetween[0] AND $priceBetween[1])";
}

if ($priceOrder == 'ascending') {
    $sqlPriceOrder = " ORDER BY `products`.`price` ASC";
}

if ($priceOrder == 'descending') {
    $sqlPriceOrder = " ORDER BY `products`.`price` DESC";
}

if (!empty($search)) {
    $sqlSearch = " AND (LOWER(brand) LIKE LOWER('%$search%') OR LOWER(name) LIKE LOWER('%$search%'))";
}

header("location: http://blog/admin?sqlPriceOrder=$sqlPriceOrder&sqlBetween=$sqlBetween&sqlSearch=$sqlSearch");
exit;