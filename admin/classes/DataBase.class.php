<?php

//namespace library;
////
////use PDO;
////use core\classes\Config;
////
////class Database {
////    private $DBH;
////
////    private static $instance;
////
////    public static function instance()
////    {
////        return self::$instance === NULL ? self::$instance = new Database : self::$instance;
////    }
////
////    private function __construct()
////    {
////        $config = Config::Load('database');
////        $this->DBH = new PDO(
////            "mysql:host={$config["host"]};port={$config["port"]};dbname={$config["dbname"]};charset={$config["charset"]}",
////            $config["user"],
////            $config["pass"],
////            [
////                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
////                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
////
////            ]
////        );
////
////    }
///
//class Database {
//
//    static $db_connect;
//
//    private static function dbParams()
//    {
//        return array(
//            'host' => 'localhost',
//            'dbname' => 'home_work',
//            'user' => 'root',
//            'password' => ''
//        );
//    }
//
//    public static function getConnect()
//    {
//
//        if (self::$db_connect === null) {
//            self::$db_connect = new PDO('mysql:host=' . self::dbParams()['host'] . ';
//        dbname=' . self::dbParams()['dbname'], self::dbParams()['user'], self::dbParams()['password']);
//
//            return self::$db_connect;
//
//        } else {
//
//            return self::$db_connect;
//
//        }
//    }
//}

namespace library;

use PDO;
use core\classes\Config;



class Db
{

    private static $instance;

    public static function getConnect(){
        if(self::$instance === NULL){
            $config = Config::Load('database');

            self::$instance = new PDO(
                "mysql:host={$config["host"]};port={$config["port"]};dbname={$config["dbname"]};charset={$config["charset"]}",
                $config["user"],
                $config["pass"],
                [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,

                ]
            );
        }else{
            return self::$instance;
        }

    }

    private  function __construct(){}
    private function __clone(){}



}

var_dump(Db::getConnect());
exit;

class Model {
    static $db;

    static function getAll($table, $where="1") {
        $stmt = DB::getConnect()->prepare("SELECT * FROM  {$table} WHERE {$where}");
        $stmt->execute();
        return $stmt->fetchAll();
    }
    static function getColomn($table, $data = [], $where="1") {
        $stmt = DB::getConnect()->prepare("SELECT ".implode(",",$data)." FROM  {$table} WHERE {$where}");
        $stmt->execute($data);
        return $stmt->fetchAll();
    }
    static function insert($table, $data){
        $keys = array_keys($data);
        $q = "INSERT INTO {$table} (`".implode("`,`",$keys)."`)";
        $q.= "VALUES (:".implode(", :",$keys).")";
        $sth = self::$db->prepare($q);
        $sth->execute($data);
        return DB::getConnect()->lastInsertId();
    }
    static function update($table, $id, $data){
        $keys = array_keys($data);
        $q = "INSERT INTO {$table} (`".implode("`,`",$keys)."`)";
        $q.= "VALUES (:".implode(", :",$keys).")";
        $sth = DB::getConnect()->prepare($q);
        $sth->execute($data);
        return DB::getConnect()->lastInsertId();
    }

}

