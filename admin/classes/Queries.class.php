<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 07.06.2018
 * Time: 11:29
 */

require 'DataBase.class.php';

class QueryBuild {
public $con;
    public function getRow($query, $params = [])
    {
        try {

            $this->con = Database::getConnect()->prepare($query);
            $stmt->execute($params);
            return $stmt->fetch();
        } catch (PDOException $e) {
            echo "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function getRows($query, $params = [])
    {
        try {
            $stmt = Database::getConnect()->prepare($query);
            $stmt->execute($params);
            return $stmt->fetchAll();
        } catch (PDOException $e) {
            echo "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}

class Table extends QueryBuild {
    public $name;
    public $description;
    public $publish;
    private $table = '';

    /**
     * Table constructor.
     * @param $name
     * @param $description
     * @param int $publish
     */
    public function __construct( $table, $name, $description, $publish = 0)
    {
        $this->name = $name;
        $this->description = $description ;
        $this->publish = $publish;
        $this->table = $table;
    }
}

$table = new Table('categories', 'name', 'description');
