<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 06.06.2018
 * Time: 14:52
 */

include 'DataBase.class.php';

class Categories extends DataBase {


    /**
     * @return array
     */
    public function getCategories() {
        try {
            $sql = "SELECT * FROM `categories`";
            $result = self::getConnect()->query($sql);
            return $result->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    /**
     * @param $name
     * @param $description
     * @param int $publish
     * @return bool
     */
    public function saveCategories($name, $description, $publish = 0)
    {
        try {
            $sql = "INSERT INTO `categories` (`name`, `description`, `publish`) VALUES (?, ?, ?)";
            $stmt = self::getConnect()->prepare($sql);
            return $stmt->execute([$name, $description, $publish]);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function delCategories($id) {
        try {
            $sql = "DELETE FROM `categories` WHERE id = " . $id;
            $stmt = self::getConnect()->prepare($sql);
            return $stmt->execute();
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function editCategories($name, $description, $publish, $id) {
        try {
            $sql = "UPDATE `categories` SET `name` = ?, `description` = ?, `publish` = ? WHERE `id` = " . $id;
            $stmt = self::getConnect()->prepare($sql);
            return $stmt->execute([$name, $description, $publish]);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}

if (!empty($_POST['submit'])) {
    $name = $_POST['name_products'];
    $description = $_POST['description_categories'];
    $publish = $_POST['publish_categories'];

    $category = new Categories();
    if ($category->saveCategories($name, $description, $publish))
        header("location: http://drugandgunshop/admin/");
}

if (!empty($_POST['edit'])) {
    $name = $_POST['name_categories'];
    $description = $_POST['description_categories'];
    $publish = $_POST['publish_categories'];
    $id = $_POST['id'];

    $category = new Categories();
    if ($category->editCategories($name, $description, $publish, $id))
        header("location: http://drugandgunshop/admin/");
}

if (!empty($_GET['id'])) {
    $id = $_GET['id'];
    $category = new Categories();
    if($category->delCategories($id))
        header("location: http://drugandgunshop/admin/");
}


