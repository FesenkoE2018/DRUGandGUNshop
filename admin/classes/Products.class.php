<?php
/**
 * Created by PhpStorm.
 * User: Admin
 * Date: 06.06.2018
 * Time: 14:53
 */

include 'DataBase.class.php';

class Products extends DataBase {


    /**
     * @return array
     */
    public function getProducts() {
        try {
            $sql = "SELECT * FROM `products`";
            $result = self::getConnect()->query($sql);
            return $result->fetchAll(PDO::FETCH_ASSOC);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    /**
     * @param $name
     * @param $description
     * @param int $publish
     * @return bool
     */
    public function saveProducts($name, $code, $brand, $image, $description, $price, $category)
    {
        try {
            $sql = "INSERT INTO `products` (`name`, `articul`, `brand`, `image_path`, `description`, `price`,
            `category_id`) VALUES (?, ?, ?, ?, ?, ?, ?)";
            $stmt = self::getConnect()->prepare($sql);
            return $stmt->execute([$name, $code, $brand, $image, $description, $price, $category]);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function editProducts($name, $code, $brand, $image, $description, $price, $category, $publish, $id) {
        try {
            $sql = "UPDATE `products` SET `name` = ?, `articul` = ?, `brand` = ?, `image_path` = ?, `description` = ?,
            `price` = ?, `category_id` = ?, `publish` = ? WHERE `id` = " . $id;
            $stmt = self::getConnect()->prepare($sql);
            return $stmt->execute([$name, $code, $brand, $image, $description, $price, $category, $publish]);
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }

    public function delProducts($id) {
        try {
            $sql = "DELETE FROM `products` WHERE id = " . $id;
            $stmt = self::getConnect()->prepare($sql);
            return $stmt->execute();
        } catch (PDOException $e) {
            print "Error!: " . $e->getMessage() . "<br/>";
            die();
        }
    }
}

if (!empty($_POST)) {
    $name = $_POST['products_name'];
    $code = $_POST['products_code'];
    $brand = $_POST['products_brand'];
    $image = $_POST['products_image'];
    $description = $_POST['products_description'];
    $price = $_POST['products_price'];
    $category = $_POST['products_category'];
    $publish = $_POST['products_publish'];
    $id = $_POST['id'];
}

if (!empty($_POST['submit'])) {
    $product = new Products();
    if ($product->saveProducts($name, $code, $brand, $image, $description, $price, $category, $publish))
        header("location: http://drugandgunshop/admin/");
}

if (!empty($_POST['edit'])) {
    $product = new Products();
    if ($product->editProducts($name, $code, $brand, $image, $description, $price, $category, $publish, $id))
        header("location: http://drugandgunshop/admin/");
}

if (!empty($_GET['id'])) {
    $id = $_GET['id'];
    $product = new Products();
    if($product->delProducts($id))
        header("location: http://drugandgunshop/admin/");
}


