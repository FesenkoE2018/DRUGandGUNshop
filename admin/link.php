<?php
/**
 * PROCEDURE METHOD
 * will be deleted after created OOP method
 */
define('SERVER', 'localhost');
define('DB_NAME', 'store');
define('USER', 'root');
define('PASSWORD', '');

$link = mysqli_connect(SERVER, USER, PASSWORD, DB_NAME);

if (mysqli_connect_errno()) {
    printf("Не удалось подключиться: %s\n", mysqli_connect_error());
    exit();
}

/**
 * OOP METHOD
 * PDO class
 */
define('DSN', 'mysql:dbname=store;host=localhost');
//define('USER', 'root');
//define('PASSWORD', '');
try {
    $pdo = new PDO(DSN, USER, PASSWORD);
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    echo "Error!: " . $e->getMessage() . "<br/>";
    die();
}