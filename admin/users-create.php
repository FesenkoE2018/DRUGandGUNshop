<?php
include_once 'DB.php';

if (!empty($_POST)) {
    $nameUsers = $_POST['name_users'];
    $emailUsers = $_POST['email_users'];
    $phoneUsers = $_POST['phone_users'];
    $passwordUsers = $_POST['password_users'];
}

/**
 *  OOP method
 *  PDO module
 *  INSERT INTO TABLE users
 */

if (isset($_POST['submit'])) {
    try {
        $sql = "INSERT INTO `users` (`name`, `email`, `phone`, `password`) VALUES (?, ?, ?, ?)";
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$nameUsers, $emailUsers, $phoneUsers, $passwordUsers]);
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
    header("location: http://blog/admin/");
    exit;
}

/**
 *  OOP method
 *  PDO module
 *  UPDATE TABLE users
 */

if (isset($_POST['edit'])) {
    try {
        $sql = "UPDATE `users` SET `name` = ?, `email` = ?, `phone` = ?, `password` = ? WHERE `id` = " . $_POST['id'];
        $stmt = $pdo->prepare($sql);
        $stmt->execute([$nameUsers, $emailUsers, $phoneUsers, $passwordUsers]);
    } catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
    header("location: http://blog/admin/");
    exit;
}

/**
 *  OOP method
 *  PDO module
 *  SELECT TABLE users
 */

if (!empty($_GET['id'])) {
    $sql = "SELECT * FROM `users` WHERE id=" . $_GET['id'];;

    try {
        $res = $pdo->query($sql);
        $row = $res->fetchAll(PDO::FETCH_ASSOC);
    }
    catch (PDOException $e) {
        print "Error!: " . $e->getMessage() . "<br/>";
        die();
    }
}

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../css/style.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <script src="../js/script.js"></script>
    <title>Add Admin-Panel</title>
</head>
<body>
<h2 class="title-admin">Users panel</h2>
<div class="container">
    <div class="products-create-tablet">
        <h3 class="products-create-tablet__text">Addition user table</h3>
        <form action="users-create.php" method="post">
            <table>
                <tr>
                    <td><label for="nameUsers">name:</label></td>
                    <td><input class="products-create__field" type="text" name="name_users"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['name'] : "" ?>" id="nameUsers"
                               required></td>
                </tr>
                <tr>
                    <td><label for="emailUsers">email:</label></td>
                    <td><input class="products-create__field" type="text" name="email_users"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['email'] : "" ?>" id="emailUsers"
                               required></td>
                </tr>
                <tr>
                    <td><label for="phoneUsers">phone:</label></td>
                    <td><input class="products-create__field" type="text" name="phone_users"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['phone'] : "" ?>" id="phoneUsers"
                               required></td>
                </tr>
                <tr>
                    <td><label for="passwordUsers">password:</label></td>
                    <td><input class="products-create__field" type="text" name="password_users"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['password'] : "" ?>" id="passwordUsersUsers"
                               required></td>
                </tr>
                <input type="hidden" name="id" value="<?= !empty($_GET['id']) ? $_GET['id'] : "" ?>">
            </table>
            <p><input type="submit" name="submit" value="Add user"><input type="submit" name="edit"
                                                                              value="Edit user"></p>
            <p><a href="http://blog/admin/" class="back-admin">back to Admin-Panel</a></p>
        </form>
    </div>

</div>
</body>
</html>