<?php
include_once 'DB.php';

if (isset($_POST['submit'])) {
    $sql = "INSERT INTO `articles` (`title`, `description`) VALUES (?, ?)";

    if ($stmt = mysqli_prepare($link, $sql)) {

        mysqli_stmt_bind_param($stmt, "ss", $titleArticles, $descriptionArticles);
        if (!$stmt->execute()) {
            die('prepare() failed: ' . htmlspecialchars($stmt->error));
        }
        mysqli_stmt_close($stmt);
        header("location: http://blog/admin/");
        exit;
    }
    mysqli_close($link);
}

if (isset($_POST['edit'])) {
    $sql = "UPDATE `articles` SET `title` = ?, `description` = ? WHERE `id` = " . $_POST['id'];
    if ($stmt = mysqli_prepare($link, $sql)) {
        mysqli_stmt_bind_param($stmt, "ss", $titleArticles, $descriptionArticles);
        if (!$stmt->execute()) {
            die('prepare() failed: ' . htmlspecialchars($stmt->error));
        }
        mysqli_stmt_close($stmt);
        header("location: http://blog/admin/");
        exit;
    }
    mysqli_close($link);
}

if (!empty($_GET['id'])) {
    $res = mysqli_query($link, "SELECT * FROM articles WHERE id=" . $_GET['id']);
    $row = mysqli_fetch_all($res, MYSQLI_ASSOC);
}

?>


<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="../css/style.css" rel="stylesheet">
    <link href="https://use.fontawesome.com/releases/v5.0.8/css/all.css" rel="stylesheet">
    <script src="../js/script.js"></script>
    <title>Add Admin-Panel</title>
</head>
<body>
<h2 class="title-admin">Articles panel</h2>
<div class="container">
    <div class="products-create-tablet">
        <h3 class="products-create-tablet__text">Addition article table</h3>
        <form action="articles-create.php" method="post">
            <table>
                <tr>
                    <td><label for="titleArticles">title:</label></td>
                    <td><input class="products-create__field" type="text" name="title_articles"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['title'] : "" ?>" id="titleArticles"
                               required></td>
                </tr>
                <tr>
                    <td><label for="descriptionArticles">description:</label></td>
                    <td><input class="products-create__field" type="text" name="description_articles"
                               value="<?= (!empty($_GET['id'])) ? $row[0]['description'] : "" ?>"
                               id="descriptionArticles"
                               required></td>
                </tr>
                <input type="hidden" name="id" value="<?= !empty($_GET['id']) ? $_GET['id'] : "" ?>">
            </table>
            <p><input type="submit" name="submit" value="Add article"><input type="submit" name="edit"
                                                                              value="Edit article"></p>
            <p><a href="http://blog/admin/" class="back-admin">back to Admin-Panel</a></p>
        </form>
    </div>

</div>
</body>
</html>
